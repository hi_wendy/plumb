# plumb

#### 介绍
记录蓝鲸magicBox流程图组件拖出10个以上图元后的bug。

解决方案：
修改dataflow.js，opt中的getId方法。注释为：// 获取流程的个数。修改为：

```javascript
var _window = $(settings.canvas).find('.jtk-window');
					var num = _window.length;
					var arr = [];
					for (var i = 0; i < num; i++) {
					    var str = $(_window).eq(i).attr('id');
					    arr.push(parseInt(str.slice(2)));
					}

					if (arr.length == 0) {
					    arr.push(0)
					};
					var _num = Math.max.apply(null, arr);
					return _num;
```
